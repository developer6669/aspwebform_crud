﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ASPWebform_CRUD._Default" %>
<%@ Register Src="~/Pages/User/_Create.ascx" TagName="Create" TagPrefix="Partial" %>
<%@ Register Src="~/Pages/User/_Read.ascx" TagName="Read" TagPrefix="Partial" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
      
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Users</h3>
                <div id = "_Create">
                    <Partial:Create ID="ee" runat="server" />
                </div>
                <div id = "_Read">
                    <Partial:Read ID="Create1" runat="server" />
                </div>
            </div>
        </div>
    </div>
<%--    <a href="#" id="demo">Load</a>
    <script>
        $("#demo").click(function () {
            alert(1);
            $.ajax({
                type: "POST",
                url: "Default.aspx/_Create",
                //data: "{message: '" + $("#message").val() + "'}",
                contentType: "application/json; charset=utf-8",                
                dataType: "json",
                success: function (r) {
                    //$("#_Create").append(r.d);
                    $("#_Create").html(r.d);
                }
            });
        });
    </script>--%>
</asp:Content>
