﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_Create.ascx.cs" Inherits="ASPWebform_CRUD.Pages.User._Create" %>

<form method="post" id="myForm">
    <div class="row">
        <div class="col-md-12 form-group">
            <label>Fisrt Name</label>
            <input type="text" class="form-control" name="FirstName" id="FirstName" />
        </div>
        <div class="col-md-12 form-group">
            <label>Last Name</label>
            <input type="text" class="form-control" name="LastName" id="LastName"  />
        </div>
        <div class="col-md-12 form-group">
            <button type="button" class="btn btn-primary" id="demo">Create</button>
        </div>
    </div>
</form>


<script>

    $("#demo").click(function () {

        var data = "{FirstName: '" + $("#FirstName").val() + "'" +
                   ",LastName: '" + $("#LastName").val() + "'}";
        $.ajax({
            enctype: 'multipart/form-data',
            url: "Default.aspx/Create",
            data: data,
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                _Read();
            }
        });
    });


</script>