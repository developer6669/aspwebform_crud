﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_Delete.ascx.cs" Inherits="ASPWebform_CRUD.Pages.User._Delete" %>

<div class="text-center">
    <h1>حذف</h1>
    <a href="#" class="btn btn-danger" onclick="Delete()">آیا مطمئن هستید ؟</a>
    <a href="#" class="btn btn-default" onclick="_Create()">انصراف</a>
</div>

<script>
    function Delete() {
        $.ajax({
            url: "Default.aspx/Delete",
            data: "{Id: '" + <%= HttpContext.Current.Session["Id"].ToString() %> + "'}",
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                _Read();
                _Create();
            }
        });
    };
</script>