﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_Read.ascx.cs" Inherits="ASPWebform_CRUD.Pages.User._Read" %>

<h1>List</h1>

<style>
    .list-user img{
        height:8rem;
        width:8rem;
    }
</style>
<div class="list-user">
    <table class="table">
        <thead>
            <tr>
                <td>#</td>
                <td>Image</td>
                <td>First Name</td>
                <td>Last Name</td>
                <td>Operation</td>
            </tr>
        </thead>
        <tbody>
             <% foreach (var item in ASPWebform_CRUD._Default.Read()) { %>

                <tr>
                    <td>1</td>
                    <td><img src="Content/images/avatar/avatar.png" /></td>
                    <td><%= item.FirstName %></td>
                    <td><%= item.LastName %></td>
                    <td>
                        <a href="#" class="btn btn-sm btn-primary" onclick="_Update('<%= item.Id %>')"><i class="fa fa-edit"></i></a>
                        <a href="#" class="btn btn-sm btn-danger" onclick="_Delete('<%= item.Id %>')"><i class="fa fa-times"></i></a>
                    </td>
                </tr>

              <% } %>
        </tbody>
    </table>
</div>

<script>

    function _Create() {

        $.ajax({
            url: "Default.aspx/_Create",
            data: "{message: '" + "y" + "'}",
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                $("#_Create").html(r.d);
            }
        });
    };

    function _Read() {

        $.ajax({
            url: "Default.aspx/_Read",
            data: "{message: '" + "y" + "'}",
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                $("#_Read").html(r.d);
            }
        });
    };


    function _Update(Id) {
        $.ajax({
            url: "Default.aspx/_Update",
            data: "{Id: '" + Id + "'}",
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                $("#_Create").html(r.d);
            }
        });
    };

    function _Delete(Id) {
        $.ajax({
            url: "Default.aspx/_Delete",
            data: "{Id: '" + Id + "'}",
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (r) {
                $("#_Create").html(r.d);
            }
        });
    };

</script>