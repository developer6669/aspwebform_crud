﻿using System;
using System.IO;
using System.Web;
using System.Web.UI;

namespace ASPWebform_CRUD
{
    public class FooHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/html";
            context.Response.Write(RenderPartialToString("Foo.ascx"));
        }

        private string RenderPartialToString(string controlName)
        {
            Page page = new Page();
            Control control = page.LoadControl(controlName);
            page.Controls.Add(control);

            StringWriter writer = new StringWriter();
            HttpContext.Current.Server.Execute(page, writer, false);

            return writer.ToString();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
