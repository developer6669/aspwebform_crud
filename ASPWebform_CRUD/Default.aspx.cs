﻿using ASPWebform_CRUD.Models.Database;
using ASPWebform_CRUD.Models.DomainModels.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ASPWebform_CRUD
{
    public partial class _Default : Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        [WebMethod] // POST
        public static string _Create()
        {
            using (Page page = new Page())
            {
                UserControl userControl = (UserControl)page.LoadControl("Pages/User/_Create.ascx");
                page.Controls.Add(userControl);
                using (StringWriter writer = new StringWriter())
                {
                    page.Controls.Add(userControl);
                    HttpContext.Current.Server.Execute(page, writer, false);
                    return writer.ToString();
                }
            }
        }

        [WebMethod] // POST
        public static string Create(string FirstName,string LastName)
        {
            DatabaseEntities db = new DatabaseEntities();

            User User = new User();

            User.FirstName = FirstName;
            User.LastName = LastName;            

            db.User.Add(User);
            db.SaveChanges();

            return "success";
        }

        [WebMethod] // POST
        public static string _Read()
        {
            using (Page page = new Page())
            {
                UserControl userControl = (UserControl)page.LoadControl("Pages/User/_Read.ascx");
                page.Controls.Add(userControl);
                using (StringWriter writer = new StringWriter())
                {
                    page.Controls.Add(userControl);
                    HttpContext.Current.Server.Execute(page, writer, false);
                    return writer.ToString();
                }
            }
        }

        public static List<User> Read()
        {
            DatabaseEntities db = new DatabaseEntities();

            return db.User.ToList();
        }

        [WebMethod] // POST
        public static string _Update(int Id)
        {
            using (Page page = new Page())
            {
                UserControl userControl = (UserControl)page.LoadControl("Pages/User/_Update.ascx");
                page.Controls.Add(userControl);

                DatabaseEntities db = new DatabaseEntities();

                var modelItem = db.User.FirstOrDefault(s => s.Id == Id);
                HttpContext.Current.Session["Id"] = Id;
                HttpContext.Current.Session["FirstName"] = modelItem.FirstName;
                HttpContext.Current.Session["LastName"] = modelItem.LastName;

                using (StringWriter writer = new StringWriter())
                {
                    page.Controls.Add(userControl);
                    HttpContext.Current.Server.Execute(page, writer, false);
                    return writer.ToString();
                }
            }
        }

        [WebMethod] // POST
        public static string Update(int Id,string FirstName, string LastName)
        {
            DatabaseEntities db = new DatabaseEntities();

            var modelUser = db.User.FirstOrDefault(s=>s.Id==Id);

            modelUser.FirstName = FirstName;
            modelUser.LastName = LastName;
            
            db.SaveChanges();

            return "success";
        }

        [WebMethod] // POST
        public static string _Delete(int Id)
        {
            using (Page page = new Page())
            {
                UserControl userControl = (UserControl)page.LoadControl("Pages/User/_Delete.ascx");                
                page.Controls.Add(userControl);
                HttpContext.Current.Session["Id"] = Id;
                using (StringWriter writer = new StringWriter())
                {
                    page.Controls.Add(userControl);
                    HttpContext.Current.Server.Execute(page, writer, false);
                    return writer.ToString();
                }
            }
        }

        [WebMethod] // POST
        public static string Delete(int Id)
        {
            DatabaseEntities db = new DatabaseEntities();

            var User = db.User.FirstOrDefault(s=>s.Id==Id);
           
            db.User.Remove(User);
            db.SaveChanges();

            return "OOO";
        }
    }
}